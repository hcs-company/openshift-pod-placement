# Usage

`kustomize build <component>/base | oc apply -f -`

or

`kustomize build <component>/overlays/<overlay> | oc apply -f -`
